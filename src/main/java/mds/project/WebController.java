/*
This is a Spring Boot controller that handles HTTP requests from a web application.
It connects to a MySQL database to authenticate user credentials and performs CRUD operations.
*/
package mds.project;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.*;

@Controller
public class WebController {

    // Database connection information
    static final String dbURL = "jdbc:mysql://localhost:3306/mds_db"; // Set in application.properties as well
    static final String user = "user"; // Username for MySQL database
    static final String pass = "user"; // Password for MySQL database

    // Display the index page
    @GetMapping("/index")
    public String index() {
        return "index";
    }

    // Authenticate user credentials and display the appropriate page
    @RequestMapping(value = "home", method = {RequestMethod.POST})
    public String form(@RequestParam String username,
                       @RequestParam String pwd,
                       Model model) throws SQLException {

        // Create a query to select the user with the provided username and password
        String query = "SELECT usrname, passwd, admin FROM mds_db.users " +
                "WHERE (usrname='" + username + "' AND passwd='" + pwd + "');";

        // Connect to the database and execute the query
        Connection conn = DriverManager.getConnection(dbURL, user, pass);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        // Check if the query returned a result
        if (rs.next()) {
            model.addAttribute("usrname",username);
            // If the user is an admin, display the admin page
            if (rs.getBoolean("admin")) {
                return "admin";
            }
            // Otherwise, display the user page
            else {
                rs.close();
                return "admin";
            }
        }
        // If the query did not return a result, display the login failed page
        else {
            rs.close();
            return "loginfailed";
        }
    }

    // Display the registration page
    @GetMapping("/registerweb")
    public String registerweb(){
        return "registerweb";
    }

    // Insert a new user into the database
    @RequestMapping(value = "register", method = {RequestMethod.POST})
    public String form(@RequestParam String username,
                       @RequestParam String pwd,
                       @RequestParam String email,
                       Model model) throws SQLException {
        try{
            // Create a query to insert the new user into the database
            String query = "INSERT INTO mds_db.users(usrname,email,passwd) VALUES " +
                    "('" + username + "' , '" + email + "', '" + pwd + "')";

            // Connect to the database and execute the query
            Connection conn = DriverManager.getConnection(dbURL, user, pass);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(query);
        }
        // If an error occurs, print the stack trace to the console
        catch (Exception e){
            System.out.println(e);
        }
        // Add the username to the model and display the admin page
        model.addAttribute("usrname",username);
        return "admin";
    }

    // Display the search page with the user's search term
    @GetMapping("search")
    public String search(@RequestParam("search") String search, Model model){
        model.addAttribute("search",search);
        return "search";
    }

    // Change a user's username in the database
    @GetMapping("changeusrname")
    public String changeusrname(Model model,
                                @RequestParam String oldusr,
                                @RequestParam String newusr) throws SQLException {
        // If either the old or new username is empty, display the login failed page
        if(oldusr.isEmpty() || newusr.isEmpty()){
            return "loginfailed";
        }

        String query ="UPDATE mds_db.users " +
                    "SET usrname = '" + newusr + "' " +
                    "WHERE usrname = '" + oldusr + "';";

        Connection conn = DriverManager.getConnection(dbURL, user, pass);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(query);
        model.addAttribute("usrname", newusr);
        return "change_succ";
    }
}
