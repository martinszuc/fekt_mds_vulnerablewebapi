# MDS Project Web Application

This is a web application project for the Multimedia services (MDS)  course. **The application is a web api that was tested against various types of attacks.** The application is built using the Spring Framework and uses a MySQL database to store user data. 

## Features

The application has the following features:

- Authentication: Users can log in with their username and password. If the user is an admin, they will be redirected to the admin page. Otherwise, they will be redirected to the user page.
- Registration: New users can register by providing their username, email, and password.
- Search: Users can search for content in the application by typing a search term in the search box.
- Change Username: Users can change their username by providing their old username and their new username.

## Setup

To set up the application, follow these steps:

1. Clone the repository to your local machine.
2. Open the project in your favorite IDE (such as Eclipse or IntelliJ IDEA).
3. Make sure that you have a MySQL database server installed on your machine.
4. Create a new database called `mds_db`.
5. Update the database connection details in the `WebController` class. Modify the `dbURL`, `user`, and `pass` constants to match your database configuration.
6. Run the application as a Spring Boot application.

## Usage

Once the application is running, you can access it by opening your web browser and navigating to http://localhost:8080/. 

From there, you can use the features of the application as described above.

## Credits

This application was developed by [Your Name]. If you have any questions or comments, feel free to contact me at [your email address].
